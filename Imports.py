import numpy as np
np.set_printoptions(threshold=10000, suppress = True)
import pandas as pd
import warnings
import matplotlib.pyplot as plt
warnings.filterwarnings('ignore')
import pre_data as p
from sklearn.model_selection import train_test_split


from sklearn.ensemble import RandomForestRegressor
from sklearn.neighbors import KNeighborsRegressor
from sklearn.neural_network import MLPRegressor
from sklearn.metrics import mean_absolute_error,accuracy_score,confusion_matrix


from sklearn.feature_extraction.text import CountVectorizer, TfidfTransformer

from sklearn.feature_extraction.text import TfidfVectorizer


from sklearn.decomposition import TruncatedSVD

import multiprocessing
import gensim
from gensim.models import Word2Vec


# from keras.preprocessing.sequence import pad_sequences
# from keras.models import Sequential
# from keras.layers import Embedding, Dense,LSTM